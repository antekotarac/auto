const Router = require("express").Router;
const cars = require("../serverData/cars.json");
const applyQueryParamsIfNeeded = require("../serverUtils/applyQueryParamsIfNeeded");

const carsRouter = new Router();

carsRouter.get("/", (req, res) => res.send(applyQueryParamsIfNeeded(cars, req.query)));

carsRouter.get("/:stockNumber", (req, res) => {
  const { stockNumber } = req.params;
  const car = cars.find((c) => c.stockNumber === parseInt(stockNumber));
  if (car) {
    return res.send({ car });
  }

  res.status(400).send("Not found");
});

module.exports = carsRouter;
