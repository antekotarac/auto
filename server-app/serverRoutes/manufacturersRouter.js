const Router = require("express").Router;
const manufacturers = require("../serverData/manufacturers.json");

const manufacturersRouter = new Router();

manufacturersRouter.get("/", (req, res) => res.send({ manufacturers }));

module.exports = manufacturersRouter;
