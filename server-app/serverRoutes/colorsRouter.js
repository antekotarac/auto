const Router = require("express").Router;
const colors = require("../serverData/colors.json");

const colorsRouter = new Router();

colorsRouter.get("/", (req, res) => res.send({ colors }));

module.exports = colorsRouter;
