const applyQueryParamsIfNeeded = (cars, query) => {
  const { color, manufacturer, page = 1, sort } = query;
  return applyPagination(applySort(applyManufacturerFilter(applyColorFilter(cars, color), manufacturer), sort), page);
};

const applyPagination = (processedCarList, page) => {
  const totalCarsCount = processedCarList.length;
  const totalPageCount = Math.ceil(totalCarsCount / 10);
  const cars = processedCarList.slice((page - 1) * 10, page * 10);
  return { cars, totalCarsCount, totalPageCount };
};

const applySort = (cars, sort) => (sort ? cars.sort((a, b) => (sort === "asc" ? a.mileage - b.mileage : b.mileage - a.mileage)) : cars);
const applyColorFilter = (cars, color) => (color ? cars.filter((car) => car.color === color) : cars);
const applyManufacturerFilter = (cars, manufacturer) => (manufacturer ? cars.filter((car) => car.manufacturerName === manufacturer) : cars);

module.exports = applyQueryParamsIfNeeded;
