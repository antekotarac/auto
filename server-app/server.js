const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const carsRouter = require("./serverRoutes/carsRouter");
const manufacturersRouter = require("./serverRoutes/manufacturersRouter");
const colorsRouter = require("./serverRoutes/colorsRouter");

app.use("/", express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/api/cars", carsRouter);
app.use("/api/manufacturers", manufacturersRouter);
app.use("/api/colors", colorsRouter);

const port = 3001;
app.listen(port, () => console.log(`Server listening on port ${port}`));
