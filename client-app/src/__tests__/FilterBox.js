import "@testing-library/jest-dom";
import React from "react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, fireEvent } from "@testing-library/react";
import FilterBox from "../component/FilterBox";

const server = setupServer(
  rest.get("/api/colors", (req, res, ctx) => {
    return res(ctx.json(["Blue", "Grey", "Silver", "Red", "White"]));
  }),
  rest.get("/api/manufacturers", (req, res, ctx) => {
    return res(ctx.json([{ name: "Fiat" }, { name: "Ford" }]));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("When filter box form is submited", () => {
  const handleFilterChange = jest.fn();
  const { container } = render(<FilterBox filterChanged={handleFilterChange} />);
  const form = container.firstChild.firstChild;
  test("should make 1 call to filterChanged handler and contain empty filters object", () => {
    fireEvent.submit(form);
    expect(handleFilterChange).toHaveBeenCalledTimes(1);
    expect(handleFilterChange).toHaveBeenCalledWith({
      colorFilter: "",
      manufacturerFilter: "",
    });
  });
});
