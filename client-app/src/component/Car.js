import React, { Component } from "react";
import { Container, Col, Row, Image } from "react-bootstrap";
import "./Car.css";
export default class Car extends Component {
  constructCompressedDesc = () => {
    return `Stock # ${this.props.stockNumber} - ${this.props.mileage.number} ${this.props.mileage.unit} - ${this.props.fuelType} - ${this.props.color}`;
  };

  constructDetailsLink() {
    return `/car/${this.props.stockNumber}`;
  }

  render() {
    return (
      <Container className="car-container">
        <Row>
          <Col xs={4} md={2}>
            <Image className="car-list-item-image" src={this.props.pictureUrl} />
          </Col>
          <Col xs={8} md={10}>
            <ul className="car-list">
              <li className="car-head-name">{this.props.modelName}</li>
              <li className="car-compressed-data">{this.constructCompressedDesc()}</li>
              <li className="car-detail-link">
                <a href={this.constructDetailsLink()} style={{ color: "orangered" }}>
                  View Details
                </a>
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    );
  }
}
