import React, { Component } from "react";
import axios from "axios";
import { Container, Row, Col, Button } from "react-bootstrap";
import "./CarDetails.css";

export default class CarDetails extends Component {
  state = {};
  async componentDidMount() {
    const { stockNumber } = this.props.match.params;
    const { data } = await axios.get(`/api/cars/${stockNumber}`);
    this.setState({ ...data.car, stockNumber, favorited: false });
  }

  constructCompressedDesc = () =>
    `Stock # ${this.state.stockNumber} - ${this.state?.mileage?.number} ${this.state.mileage?.unit} - ${this.state.fuelType} - ${this.state.color}`;

  addToFavorites = () => {
    let favorites = localStorage.favorites;
    const { stockNumber } = this.state;
    favorites = favorites ? JSON.parse(favorites) : [];
    if (!favorites.includes(stockNumber)) {
      favorites.push(stockNumber);
      localStorage.setItem("favorites", JSON.stringify(favorites));
      this.setState({ favorited: true });
    }
  };

  removeFromFavorites = () => {
    let favorites = localStorage.favorites;
    const { stockNumber } = this.state;
    favorites = favorites ? JSON.parse(favorites) : [];
    if (favorites.includes(stockNumber)) {
      favorites = favorites.filter((s) => s !== stockNumber);
      localStorage.setItem("favorites", JSON.stringify(favorites));
      this.setState({ favorited: false });
    }
  };

  renderFavoritesSection = () => {
    let favorites = localStorage.favorites;
    const { stockNumber } = this.state;
    favorites = favorites ? JSON.parse(favorites) : [];
    if (favorites.includes(stockNumber)) {
      return (
        <Button onClick={this.removeFromFavorites} className="car-details-favorites-button" variant="danger" type="submit" size="sm">
          Remove
        </Button>
      );
    } else {
      return (
        <Button onClick={this.addToFavorites} className="car-details-favorites-button" variant="danger" type="submit" size="sm">
          Save
        </Button>
      );
    }
  };

  render() {
    return (
      <Container fluid>
        <Row>
          <a href="/" style={{ color: "blue" }}>
            Go Back
          </a>
        </Row>
        <Row>
          <Col xs={12} md={8}>
            <div className="car-details-head-name">{this.state.modelName}</div>
            <div className="car-details-compressed-desc">{this.constructCompressedDesc()}</div>

            <p className="car-details-desc">
              This car is currently available and can be delivered as soon as tomorrow morning. Please be aware that delivery times shown in this page
              are not definitive and may change due to bad weather conditions.
            </p>
          </Col>
          <Col xs={12} md={4}>
            <Container className="border border-dark">
              <Row className="car-details-favorites-box">
                If you like this car, click the button and save it in your collection of favourite items
              </Row>
              <Row className="car-details-favorites-box">
                <Col></Col>
                <Col xs={4}>{this.renderFavoritesSection()}</Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}
