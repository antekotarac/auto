import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col } from "react-bootstrap";
import FilterBox from "./FilterBox";
import Cars from "./Cars";
class Home extends Component {
  state = {
    carsFilter: {},
  };

  filterApply = (carsFilter) => {
    this.setState({ carsFilter });
  };

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            <FilterBox filterChanged={this.filterApply} />
          </Col>
          <Col xs={12} md={8}>
            <Cars filter={this.state.carsFilter} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;
