import React, { Component } from "react";
import axios from "axios";
import { Container, Row, Col, Pagination } from "react-bootstrap";
import Car from "./Car";

export default class CarList extends Component {
  state = { cars: [], page: 1, totalPageCount: 1, totalCarsCount: 0 };

  async componentDidMount() {
    const { data } = await axios.get(`/api/cars?page=${1}${this.constructFilterIfNeeded()}`);
    const { cars, totalCarsCount, totalPageCount } = data;
    this.setState({ cars, totalCarsCount, totalPageCount });
  }

  constructFilterIfNeeded() {
    let filter = "";
    const { colorFilter, manufacturerFilter } = this.props.filter;
    filter += colorFilter ? `&color=${colorFilter}` : "";
    filter += manufacturerFilter ? `&manufacturer=${manufacturerFilter}` : "";
    return filter;
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.filter.colorFilter !== this.props.filter.colorFilter ||
      prevProps.filter.manufacturerFilter !== this.props.filter.manufacturerFilter
    ) {
      const { data } = await axios.get(`/api/cars?page=${1}${this.constructFilterIfNeeded()}`);
      const { cars, totalCarsCount, totalPageCount } = data;
      this.setState({ cars, totalCarsCount, totalPageCount, page: 1 });
    }
  }

  constructPageRatioString = () => `Page ${this.state.page} of ${this.state.totalPageCount || 1}`;
  constructUrl = (page) => `/api/cars?page=${page}${this.constructFilterIfNeeded()}`;

  async setPage(page) {
    page = page < 1 ? 1 : page;
    page = page > this.state.totalPageCount ? this.state.totalPageCount : page;
    if (page === this.state.page) return;
    const { data } = await axios.get(this.constructUrl(page));
    const { cars, totalCarsCount, totalPageCount } = data;
    this.setState({ cars, totalCarsCount, totalPageCount, page });
  }

  render() {
    return (
      <div>
        {this.state.cars.map((c) => (
          <Car key={Math.random()} {...c} />
        ))}
        <footer>
          <Container>
            <Row>
              <Col></Col>
              <Col xs={8}>
                <Pagination size="md">
                  <Pagination.First onClick={() => this.setPage(1)}>First</Pagination.First>
                  <Pagination.Prev onClick={() => this.setPage(this.state.page - 1)}>Previous</Pagination.Prev>
                  <Pagination.Item disabled>{this.constructPageRatioString()}</Pagination.Item>
                  <Pagination.Next onClick={() => this.setPage(this.state.page + 1)}>Next</Pagination.Next>
                  <Pagination.Last onClick={() => this.setPage(this.state.totalPageCount)}>Last</Pagination.Last>
                </Pagination>
              </Col>
              <Col></Col>
            </Row>
          </Container>
        </footer>
      </div>
    );
  }
}
