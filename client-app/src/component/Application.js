import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Application.css";
import Home from "./Home";
import CarDetails from "./CarDetails";

class Application extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/car/:stockNumber" component={CarDetails} />
      </Router>
    );
  }
}

export default Application;
