import React from "react";
import { Container } from "react-bootstrap";
import CarList from "./CarList";

const Cars = ({ filter }) => {
  return (
    <Container>
      <h3>
        <b>Available cars</b>
      </h3>
      <h3>Showing 10 of 100 results</h3>
      <CarList filter={filter} />
    </Container>
  );
};

export default Cars;
