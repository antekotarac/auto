import React, { Component } from "react";
import { Form, Button, Container } from "react-bootstrap";
import axios from "axios";
import "./FilterBox.css";
export default class FilterBox extends Component {
  state = { colors: [], manufacturers: [], colorFilter: "", manufacturerFilter: "" };
  componentDidMount() {
    Promise.all([axios.get("/api/colors"), axios.get("/api/manufacturers")]).then((values) => {
      const { colors } = values[0].data;
      const { manufacturers } = values[1].data;
      this.setState({ colors, manufacturers: manufacturers.map((m) => m.name) });
    });
  }

  colorChange = (e) => this.setState({ colorFilter: e.target.value });
  manufacturerChange = (e) => this.setState({ manufacturerFilter: e.target.value });
  generateColorList = () => this.state.colors.map((c) => <option key={c}>{c}</option>);
  generateManufacturerList = () => this.state.manufacturers.map((m) => <option key={m}>{m}</option>);

  emitFilter = (e) => {
    e.preventDefault();
    if (this.props.filterChanged) {
      let { colorFilter, manufacturerFilter } = this.state;
      colorFilter = colorFilter === "All colors" ? "" : colorFilter;
      manufacturerFilter = manufacturerFilter === "All manufacturers" ? "" : manufacturerFilter;
      this.props.filterChanged({ colorFilter, manufacturerFilter });
    }
  };

  render() {
    return (
      <Container>
        <Form onSubmit={this.emitFilter}>
          <Form.Group>
            <Form.Label>Color</Form.Label>
            <Form.Control as="select" data-testid="colorSelect" onChange={this.colorChange}>
              <option>All colors</option>
              {this.generateColorList()}
            </Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label>Manufacturer</Form.Label>
            <Form.Control as="select" onChange={this.manufacturerChange}>
              <option>All manufacturers</option>
              {this.generateManufacturerList()}
            </Form.Control>
          </Form.Group>
          <Button className="float-right pad-side" variant="danger" type="submit" size="sm">
            Filter
          </Button>
        </Form>
      </Container>
    );
  }
}
